<?php
session_start();
$solicitacao = $_SESSION['solicitacao'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sucesso</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col"><h2>Sucesso ao solicitar</h2></div>
        </div>
        <div class="row">
            <div class="col"><h3>Anote seu protocolo de atendimento: <?php echo $solicitacao;?></h3></div>
        </div>
        <div class="row">
            <div class="col">
                <form action="../index.php"><button class="btn btn-secondary" type="submit">OK</button></form>
            </div>
        </div>
    </div>
</body>
</html>