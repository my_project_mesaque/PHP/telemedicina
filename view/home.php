<?php
session_start();
if (!isset($_SESSION['user'])) 
    header("location:login.php");
require_once __DIR__ . "/../model/dao/consultaDAO.php";
$dao = new ConsultaDAO();
$consulta = $dao->getAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
    <div class="container">
        <form action="../controller/logoutController.php"><button class="btn btn-secondary" type="submit">Sair</button></form>
        <br>
        <?php
        while($test=mysqli_fetch_assoc($consulta)){
            $rua = $test['rua'];
            $nome = $test['nome'];
            $desc = $test['desc_sintomas'];
            $id = $test['id'];
            
            echo "<div class=\"row\">";
            echo "  <div class=\"col\">";
            echo "      <div class=\"card-body\">";
            echo "          <h5 class=\"card-title\">".$nome."</h5>";
            echo "          <h6 class=\"card-subtitle mb-2 text-muted\">".$rua."</h6>";
            echo "          <p class=\"card-text\">".$desc."</p>";
            echo "          <a href=\"mensagens.php?id=".$id."\" class=\"card-link\">Consultar</a>";
            echo "          <a href=\"detalhesPaciente.php?id=".$id."\" class=\"card-link\">Detalhes</a>";
            echo "      </div>";
            echo "  </div>";
            echo "</div>";
        }
        ?>

    </div>
</div>
</body>
</html>