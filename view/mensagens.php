<?php
session_start();

if (!isset($_SESSION['user'])) 
    header("location:login.php");


$id = "";
if(isset($_POST['id'])){
    $id = $_POST['id'];
}else if(isset($_GET['id'])){
    $id = $_GET['id'];
}else{
    $id = $_SESSION['id_t'];
}
require_once __DIR__ . "/../model/dao/consultaDAO.php";
$dao = new ConsultaDAO();
$consulta = $dao->getAllMensagens($id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<div class="container">
    <div class="fixed-top">
        <a href="home.php" style="margin-right: 5%;">< Voltar</a>
        <?php
        echo "<a href=\"../controller/encerrarConsultaController.php?id=\"".$id."style=\"margin-left: 5%;\">Encerrar consulta</a>"
        ?>
    </div>
    <div class="row" style="margin-bottom: 10%; margin-top: 4%;">
        <div class="col">   
            
            <?php
                while($res=mysqli_fetch_assoc($consulta)){
                    $msge = $res['mensagem'];
                    echo "<div class=\"card bg-light mb-3\" style=\"margin: 1%;\">";
                    echo "<div class=\"card-body\">";
                    echo "  <p class=\"card-text\">".$msge."</p>";
                    echo "</div>";
                    echo "</div>";
                }
            ?>
        </div>
    </div>
    <div class="fixed-bottom">
        <div class="col">
            <nav class="navbar navbar-dark bg-primary">
                <div class="row" style="width: 100%">
                    <div class="col-12">
                        <form action="../controller/enviarMensagemController.php" method="post" style="width: 100%">
                            <div class="form-row">
                                <div class="col-11">
                                    <textarea class="form-control" style="size: 100%" rows="1" name="msgM"></textarea>
                                </div>
                                <div class="col-1">
                                    <?php 
                                    echo "<input type=\"hidden\" name=\"id\" value=\"".$id."\"/>";
                                    ?>
                                    <button class="btn btn-primary" type="submit">Enviar</button>
                                </div>
                            </div>                    
                        </form>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
</body>
</html>