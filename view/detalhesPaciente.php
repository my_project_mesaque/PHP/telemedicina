<?php
session_start();
if (!isset($_SESSION['user'])) 
    header("location:login.php");
$id = "";
if(isset($_GET['id'])){
    $id = $_GET['id'];
}
require_once __DIR__ . "/../model/entity/Consulta.php";
$consulta = new Consulta();
require_once __DIR__ . "/../model/dao/consultaDAO.php";
$dao = new ConsultaDAO();
$consulta = $dao->getById($id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detalhes do Paciente</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="home.php">< Voltar</a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <?php
                echo "<b>NOME: </b>". $consulta->getNome();
                echo "<br><b>IDADE: </b>". $consulta->getIdade();
                echo "<br><b>NÚMERO DO SUS: </b>". $consulta->getNum_sus();
                echo "<br><b>ENDEREÇO: </b>". $consulta->getRua();
                echo "<br><b>TELEFONE PARA CONTATO: </b>". $consulta->getTelefone();
                echo "<br><b>DESCRIÇÃO DOS SINTOMAS: </b>". $consulta->getDesc_sitomas();
                ?>         
            </div>
        </div>
    </div>
</body>
</html>