<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login - Médico</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<div class="container">
<br>
    <form action="../index.php"><button class="btn btn-secondary" type="submit">Início</button></form>
    <br>
    <form action="../controller/loginController.php" method="POST">
        <div class="form-row">
            <div class="col">
                <input type="text" class="form-control" placeholder="Usuario" name="usuario" required>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <input type="password" class="form-control" placeholder="Senha" name="senha" required>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <button class="btn btn-primary" type="submit">Login</button>
            </div>
        </div>
    </form>
</div> 
</body>
</html>