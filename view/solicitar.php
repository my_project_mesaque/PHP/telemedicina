<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<div class="container">
    <br>
    <form action="../index.php"><button class="btn btn-secondary" type="submit">Início</button></form>
<br><br>
    <form method="POST" action="../controller/solicitarController.php">
        <div class="form-row">
            <div class="col-7">
                <input type="text" class="form-control" placeholder="Nome Completo" name="nome">
            </div>
            <div class="col-5">
                <input type="text" class="form-control" placeholder="Idade" name="idade">
            </div>
        </div>      
        <br>          
        <div class="form-row">
            <div class="col">
                <input type="text" class="form-control" placeholder="Endereço" name="endereco">
            </div>
        </div>   
        <br>         
        <div class="form-row">
            <div class="col">
                <input type="text" class="form-control" placeholder="Nº do SUS" name="sus">
            </div>
            <div class="col">
                <input type="text" class="form-control" placeholder="Telefone Ex.: 996969696" name="telefone">
            </div>
        </div>    
        <br>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Descrição dos sintomas</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="4" name="descri"></textarea>
        </div>   
        <div class="form-row">
            <div class="col">
                <button class="btn btn-primary" type="submit">Solicitar atendimento</button>
            </div>
        </div>
    </form>    
</div>
</body>
</html>