<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Protocolo de atendimento</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<div class="container">
    <br>
    <form action="../index.php"><button class="btn btn-secondary" type="submit">Início</button></form>
    <br><br>
    <form action="../controller/acompanhamentoController.php" method="post">
        <div class="form-row">
            <div class="col">
                <input type="text" class="form-control" placeholder="Numero do protocolo" name="protocolo">
            </div>
        </div>
        <br>
        <div class="form-row">
            <div class="col">
                <button class="btn btn-primary" type="submit">Acompanhar atendimento</button>
            </div>
        </div>    
    </form>
</div>
</body>
</html>