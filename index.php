<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="view/css/bootstrap.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col">
            <p><a href="view/login.php">Login</a></p>
        </div>

    </div>
    <div class="row">
        <div class="col">
            <form action="view/solicitar.php" method="get">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Solicitar nova consulta</button>
            </form>            
        </div>
        <div class="col">
            <form action="view/protocolo.php" method="get">
                <button type="submit" class="btn btn-secondary btn-lg btn-block">Acompanhar consulta</button>
            </form>
        </div>
    </div>    
</div>
</body>
</html>