<?php
class Consulta{
    private $id_consulta;
    private $nome;
    private $idade;
    private $rua;
    private $num_sus;
    private $telefone;
    private $desc_sitomas;
    private $status;

    function setId_consulta($id_consulta){
        $this->id_consulta = $id_consulta;
    }
    function getId_consulta(){
        return $this->id_consulta;
    }

    function setNome($nome){
        $this->nome = $nome;
    }
    function getNome(){
        return $this->nome;
    }

    function setIdade($idade){
        $this->idade = $idade;
    }
    function getIdade(){
        return $this->idade;
    }

    function setRua($rua){
        $this->rua = $rua;
    }
    function getRua(){
        return $this->rua;
    }

    function setNum_sus($num_sus){
        $this->num_sus = $num_sus;
    }
    function getNum_sus(){
        return $this->num_sus;
    }

    function setTelefone($telefone){
        $this->telefone = $telefone;
    }
    function getTelefone(){
        return $this->telefone;
    }

    function setDesc_sitomas($desc_sitomas){
        $this->desc_sitomas = $desc_sitomas;
    }
    function getDesc_sitomas(){
        return $this->desc_sitomas;
    }

    function setStatus($status){
        $this->status = $status;
    }
    function getStatus(){
        return $this->status;
    }
}
?>