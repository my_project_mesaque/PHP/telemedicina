<?php
include(dirname(__FILE__) . '/../connection/connection.php');

class ConsultaDAO{
    private $sql;

    function saveC(Consulta $consulta){
        try {
            $id = $consulta->getId_consulta();
            $nome = $consulta->getNome();
            $idade = $consulta->getIdade();
            $rua = $consulta->getRua();
            $num_sus = $consulta->getNum_sus();
            $telefone = $consulta->getTelefone();
            $desc_sitomas = $consulta->getDesc_sitomas();
            $status = $consulta->getStatus();
            $this->sql = "INSERT INTO `consultas` (`id`, `nome`, `idade`, `rua`, `num_sus`, `telefone`, `desc_sintomas`, `status`) VALUES ('$id','$nome', '$idade', '$rua', '$num_sus', '$telefone', '$desc_sitomas', '$status');";
            global $connection;
            $query = mysqli_query($connection, $this->sql);
            return true;
        } catch (\Throwable $th) {
            die("Error!");
            return false;
        }           
    }

    function envMesangem($mensagem, $id){
        try {
            $this->sql = "INSERT INTO `mensagens` (`id_consulta`, `mensagem`) VALUES ('$id', '$mensagem');";
            global $connection;
            $query = mysqli_query($connection, $this->sql);
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }

    function alterarStatus($id_consulta){
        try {
            $this->sql = "UPDATE `consultas` SET `status` = 1 WHERE `consultas`.`id` = $id_consulta;";
            global $connection;
            $query = mysqli_query($connection, $this->sql);
            return true;
        } catch (\Throwable $th) {
            return false;
        }        
    }

    function getAllMensagens($id_consulta){
        $sql = "SELECT * FROM `mensagens` WHERE id_consulta='$id_consulta'";
        global $connection;
        $result = mysqli_query($connection, $sql);
        return $result;
    }
    function getAll(){
        $sql = "SELECT * FROM `consultas` WHERE status=0";
        global $connection;
        $result = mysqli_query($connection, $sql);
        return $result;
    }
    function getById($id_consulta){
        require_once __DIR__ . "/../entity/Consulta.php";
        $con = new Consulta();
        $sql_atend = "SELECT * FROM `consultas` WHERE id='$id_consulta' AND status=0";
        global $connection;
        $result = mysqli_query($connection, $sql_atend);
        $count = mysqli_num_rows($result);

        if($count==1){
            $test=mysqli_fetch_assoc($result);        
            $con->setId_consulta($id_consulta);
            $con->setNome($test['nome']);
            $con->setRua($test['rua']);
            $con->setNum_sus($test['num_sus']);
            $con->setTelefone($test['telefone']);
            $con->setDesc_sitomas($test['desc_sintomas']);
            $con->setStatus($test['status']);
            $con->setIdade($test['idade']);
        }else{
            $con = null;
        }        
        return $con;
    }
}
?>