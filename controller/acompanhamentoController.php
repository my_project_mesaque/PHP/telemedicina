<?php
$id = $_POST['protocolo'];
require_once __DIR__ . "/../model/dao/consultaDAO.php";
$dao = new ConsultaDAO();
$consulta = $dao->getById($id);
if(is_null($consulta)){
    header("location:../view/protocolo.php");
}else{
    session_start();
    $_SESSION['protocolo'] = $id;
    header("location:../view/homePaciente.php");
}
?>