<?php
session_start();
$solicitacao = $nome = $idade = $endereco = $sus = $telefone = $desc = "";

if (isset($_POST["nome"])) {
    $nome = $_POST["nome"];
} 
if (isset($_POST["idade"])) {
    $idade = $_POST["idade"];
} 
if (isset($_POST["endereco"])) {
    $endereco = $_POST["endereco"];
} 
if (isset($_POST["sus"])) {
    $sus = $_POST["sus"];
} 
if (isset($_POST["telefone"])) {
    $telefone = $_POST["telefone"];
} 
if (isset($_POST["descri"])) {
    $desc = $_POST["descri"];
} 

require_once __DIR__ . '/../model/service/Gerador.php';
$ticket = new Gerador();
$solicitacao = $ticket->getValue();
$_SESSION['solicitacao'] = $solicitacao;
require_once __DIR__ . '/../model/entity/Consulta.php';
$consulta = new Consulta();
$consulta->setId_consulta($solicitacao);
$consulta->setNome($nome);
$consulta->setIdade($idade);
$consulta->setRua($endereco);
$consulta->setNum_sus($sus);
$consulta->setTelefone($telefone);
$consulta->setDesc_sitomas($desc);
$consulta->setStatus(0);

require_once __DIR__ . '/../model/dao/consultaDAO.php';
$dao = new ConsultaDAO();
$tes = $dao->saveC($consulta);

header("location:../view/sucesso.php");
?>