<?php
$msg = $id = "";
if(isset($_POST['msgM'])){
    $msg = "<b>MÉDICO:</b> ";
    $msg .= $_POST['msgM'];
}else if(isset($_POST['msgP'])){
    $msg = "<b>PACIENTE:</b> ";
    $msg .= $_POST['msgP'];
}
if(isset($_POST['id'])){
    $id = $_POST['id'];
}
require_once __DIR__ . "/../model/dao/consultaDAO.php";
$dao = new ConsultaDAO();
$save = $dao->envMesangem($msg, $id);
if($save==true){
    if(isset($_POST['msgM'])){
        $link = "location:../view/mensagens.php?id=".$id;
        header($link);
    }else{
        $link = "location:../view/homePaciente.php?id=".$id;
        header($link);
    }
}else{
    echo "error!";
}
?>